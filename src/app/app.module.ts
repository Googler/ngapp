import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MynavComponent } from './mynav/mynav.component';
import { MyfooterComponent } from './myfooter/myfooter.component';


@NgModule({
  declarations: [
    AppComponent,
    MynavComponent,
    MyfooterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
